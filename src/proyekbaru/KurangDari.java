package proyekbaru;

public class KurangDari {

    public static void main(String[] args) {
        int a = 10;
        int b = 15;
        System.out.println("Variabel a : " + a);
        System.out.println("Variabel b : " + b);
        System.out.println("Apakah 10 kurang dari 15 " + (a < b));
        System.out.println("Apakah 15 kurang dari 10 " + (b < a));
        System.out.println("Apakah 15 kurang dari 15 " + (b < b));
    }
}
