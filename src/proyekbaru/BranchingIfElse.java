package proyekbaru;

import javax.swing.JOptionPane;

public class BranchingIfElse {

    public static void main(String[] args) {
        String temp = JOptionPane.showInputDialog(null, "Masukan Angka", "Menentukan Ganjil Genap", JOptionPane.QUESTION_MESSAGE);
        int input = Integer.parseInt(temp);
        System.out.println((input % 2));
        if ((input % 2) == 0) {
            JOptionPane.showMessageDialog(null, "Angka : " + input + " "
                    + "adalah bilangan GENAP", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        } else if ((input % 2) == 1) {
            JOptionPane.showMessageDialog(null, "Angka : " + input + " "
                    + "adalah bilangan GANJIL", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
