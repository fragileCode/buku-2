package proyekbaru;

import javax.swing.JOptionPane;

public class PlainMessage {

    public static void main(String[] args) {
        int pertanyaan = 
                JOptionPane.showConfirmDialog(null, "Pilih Yes atau No ?",
                        "Pertanyaan",JOptionPane.YES_NO_OPTION);
        
        if(JOptionPane.YES_OPTION == pertanyaan){
            JOptionPane.showMessageDialog(null, "Kamu memilih Yes.");
        }else if(JOptionPane.NO_OPTION == pertanyaan){
            JOptionPane.showMessageDialog(null, "Kamu memilih No.");
        }
    }
    
}
