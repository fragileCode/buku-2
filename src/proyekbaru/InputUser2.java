package proyekbaru;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputUser2 {
    
    public static void main(String[] args) 
            throws IOException {
        String namaDepan = "";
        String namaBelakang = "";
        
        //buat objek dari inputStream
        InputStreamReader ireader = 
                new InputStreamReader(System.in);
        //buat objek bufferreader
        BufferedReader breader = 
                new BufferedReader(ireader);
        
        System.out.println("Nama Depan : ");
        //membaca inputan user
        namaDepan = breader.readLine();
        System.out.println("Nama Belakang : ");
        //membaca inputan user
        namaBelakang = breader.readLine();
        System.out.println("Nama Saya adalah :");
        System.out.println(namaDepan + " " + namaBelakang);
    }
}
