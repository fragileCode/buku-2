package proyekbaru;

import java.util.Scanner;

public class InputUser1 {

    public static void main(String[] args) {
        String namaDepan = "";
        String namaBelakang = "";

        Scanner input = new Scanner(System.in);
        
        System.out.println("Nama Depan : ");
        //membaca inputan user
        namaDepan = input.next();
        System.out.println("Nama Belakang : ");
        //membaca inputan user
        namaBelakang = input.next();
        System.out.println("Nama Saya adalah :");
        System.out.println(namaDepan + " " + namaBelakang);
    }

}
