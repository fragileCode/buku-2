package proyekbaru;

import javax.swing.JOptionPane;

public class BranchingIfElse2 {

    public static void main(String[] args) {
        String umur = JOptionPane.showInputDialog(null, "Kamu umur berapa ?");

        if (Integer.parseInt(umur) <= 5) {
            JOptionPane.showMessageDialog(null, "Balita");
        } else if (Integer.parseInt(umur) >= 6 && Integer.parseInt(umur) <= 11) {
            JOptionPane.showMessageDialog(null, "Anak - anak");
        } else if (Integer.parseInt(umur) >= 12 && Integer.parseInt(umur) <= 25) {
            JOptionPane.showMessageDialog(null, "Remaja");
        } else {
            JOptionPane.showMessageDialog(null, "Dewasa");
        }
    }
}
