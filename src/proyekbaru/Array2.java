package proyekbaru;

public class Array2 {

    public static void main(String[] args) {
        String nama[] = new String[3];
        nama[0] = "Budi";
        nama[1] = "Andi";
        nama[2] = "Dwi";
        
        System.out.println("Index 0 : "+nama[0]);
        System.out.println("Index 1 : "+nama[1]);
        System.out.println("Index 2 : "+nama[2]);
    }

}
