package proyekbaru;

import javax.swing.JOptionPane;

public class InputUser4 {
    
    public static void main(String[] args) {
        String a = 
                JOptionPane.showInputDialog("Masukan nilai untuk variabel a : ");
        
        String b = 
                JOptionPane.showInputDialog("Masukan nilai untuk variabel b : ");
        
        System.out.println("Hasil :" +
                (Integer.parseInt(a) * Integer.parseInt(b)));
    }
    
}
