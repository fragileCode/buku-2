package proyekbaru;

public class Negasi {

    public static void main(String[] args) {
        int a = 2;
        int b = 0;

        System.out.println("Sebelum negasi : " + a);
        b = -a;
        System.out.println("Sebelum negasi : " + b);
    }
}
