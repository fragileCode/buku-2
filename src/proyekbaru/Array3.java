package proyekbaru;

import javax.swing.JOptionPane;

public class Array3 {

    public static void main(String[] args) {
        String[] nama = new String[3];
        nama[0] = JOptionPane.showInputDialog(null,"Nama :","index ke-0 ",JOptionPane.INFORMATION_MESSAGE);
        nama[1] = JOptionPane.showInputDialog(null,"Nama :","index ke-1 ",JOptionPane.INFORMATION_MESSAGE);
        nama[2] = JOptionPane.showInputDialog(null,"Nama :","index ke-2 ",JOptionPane.INFORMATION_MESSAGE);
        
        JOptionPane.showMessageDialog(null, nama[0]+", "+nama[1]+", "+nama[2]);
    }

}
