package proyekbaru;

public class SamaDenganTidakSamaDengan {

    public static void main(String[] args) {
        int a = 3;
        int b = 3;
        System.out.println("Variabel a : " + a);
        System.out.println("Variabel b : " + b);
        System.out.println("Apakah 3 sama dengan 3 " + (a == b));
        a = 2;
        System.out.println("Apakah 2 sama dengan 3 " + (a == b));
        System.out.println("Apakah 2 tidak sama dengan 3 " + (a != b));
    }

}
