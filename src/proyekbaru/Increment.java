package proyekbaru;

public class Increment {

    public static void main(String[] args) {
        Integer sebelum = 20;
        Integer sesudah = 0;

        System.out.println("Sebelum Increment : " + sebelum);
        sesudah = ++sebelum;
        System.out.println("Sesudah Increment : " + sesudah);
    }

}
