package proyekbaru;

import java.math.BigDecimal;

public class Konversi1 {

    public static void main(String[] args) {
        String var1 = "1234567899";
        String var2 = "3.14";
        String var3 = "1";

        int bulat = Integer.parseInt(var1);
        double pecahan = Double.parseDouble(var2);
        short bulat2 = Short.parseShort(var3);
        
        System.out.println("variabel var1 " + var1);
        System.out.println("variabel var2 " + var2);
        System.out.println("variabel var3 " + var3);
        System.out.println("---");
        System.out.println("konversi bilangan bulat " + bulat);
        System.out.println("konversi bilangan pecahan " + pecahan);
        System.out.println("konversi bilangan bulat2 " + bulat2);
    }
}
