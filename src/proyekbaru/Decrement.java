package proyekbaru;

public class Decrement {

    public static void main(String[] args) {
        Integer sebelum = 9;
        Integer sesudah = 0;

        System.out.println("Sebelum Decrement : " + sebelum);
        sesudah = --sebelum;
        System.out.println("Sesudah Decrement : " + sesudah);
    }
}
