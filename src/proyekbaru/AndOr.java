package proyekbaru;

public class AndOr {

    public static void main(String[] args) {
        System.out.println("true and true : " + (true && true));
        System.out.println("true and false : " + (true && false));
        System.out.println("false and false : " + (false && false));
        System.out.println("false and true : " + (false && true));
        System.out.println("-------------------------------------------------");
        System.out.println("true or true : " + (true || true));
        System.out.println("true or false : " + (true || false));
        System.out.println("false or false : " + (false || false));
        System.out.println("false or true : " + (false || true));
    }
}
