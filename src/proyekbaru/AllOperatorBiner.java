package proyekbaru;

public class AllOperatorBiner {

    public static void main(String[] args) {
        //Penambahan
        int a1 = 10;
        int a2 = 2;
        System.out.println("Penambahan 10 + 2 : " + (a1 + a2));

        //Pengurangan
        int b1 = 10;
        int b2 = 2;
        System.out.println("Pengurangan 10 - 2 : " + (b1 - b2));
        
        //Perkalian
        int c1 = 16;
        int c2 = 2;
        System.out.println("Perkalian 16 * 2 : " + (c1 * c2));
        
        //Pembagian
        int d1 = 16;
        int d2 = 2;
        System.out.println("Pembagian 16 / 2 : " + (d1 / d2));
        
        //Sisa Bagi
        int e1 = 16;
        int e2 = 2;
        System.out.println("Sisa bagi 16 dan 2 : " + (e1 %e2));
    }
}
