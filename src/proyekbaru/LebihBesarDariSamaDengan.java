package proyekbaru;

public class LebihBesarDariSamaDengan {

    public static void main(String[] args) {
        int a = 3;
        int b = 3;
        System.out.println("Variabel a : " + a);
        System.out.println("Variabel b : " + b);
        System.out.println("Apakah 3 lebih besar atau sama dengan 3 " + (a >= b));
        a = 2;
        System.out.println("Variabel a diubah : " + a);
        System.out.println("Apakah 2 lebih besar atau sama dengan 3 " + (a >= b));
    }
}
