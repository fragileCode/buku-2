package proyekbaru;

import javax.swing.JOptionPane;

public class LoopingFor4 {

    public static void main(String[] args) {
        int jumlahKelas = Integer.parseInt(JOptionPane.showInputDialog(null, "Masukan jumlah kelas : ",
                "Input", JOptionPane.INFORMATION_MESSAGE));
        String isiKelas[][] = new String[jumlahKelas][];

        for (int kelas = 0; kelas < jumlahKelas; kelas++) {
            String namaKelas = JOptionPane.showInputDialog(null, "Nama Kelas :", "Masukan Nama Kelas", JOptionPane.INFORMATION_MESSAGE);
            int jumlahKursi = Integer.parseInt(JOptionPane.showInputDialog(null, "Jumlah murid Kelas " + namaKelas + " : ",
                    "Input", JOptionPane.INFORMATION_MESSAGE));
            isiKelas[kelas] = new String[jumlahKursi];
            System.out.println("Kelas " + namaKelas + " : ");
            for (int kursi = 0; kursi < jumlahKursi; kursi++) {
                String namaSiswa = JOptionPane.showInputDialog(null, "Nama Siswa", "Kelas " + namaKelas, JOptionPane.INFORMATION_MESSAGE);
                isiKelas[kelas][kursi] = namaSiswa;
                System.out.println("Nama Siswa " + isiKelas[kelas][kursi]);
            }
            System.out.println("----------------------------------------");
        }
    }

}
