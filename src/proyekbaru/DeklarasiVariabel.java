package proyekbaru;

public class DeklarasiVariabel {

    public static String variabel1;
    public static String variabel2;

    public static void main(String[] args) {
        System.out.println("variabel1 -> " + variabel1);
        System.out.println("variabel2 -> " + variabel2);
        System.out.println("setelah diisi :");
        variabel1 = "Hello";
        variabel2 = "Word";
        System.out.println("variabel1 -> " + variabel1);
        System.out.println("variabel2 -> " + variabel2);
    }

}
