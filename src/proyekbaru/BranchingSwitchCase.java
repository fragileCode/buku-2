package proyekbaru;

import javax.swing.JOptionPane;

public class BranchingSwitchCase {

    public static void main(String[] args) {
        String nilai = JOptionPane.showInputDialog(null, "Masukan nilai :");
        switch (nilai.toUpperCase()) {
            case "A":
                JOptionPane.showMessageDialog(null, "Sangat baik.");
                break;
            case "B":
                JOptionPane.showMessageDialog(null, "Baik.");
                break;
            case "C":
                JOptionPane.showMessageDialog(null, "Cukup.");
            case "D":
                JOptionPane.showMessageDialog(null, "Jelek.");
                break;
            default :
                JOptionPane.showMessageDialog(null, "Salah memasukan nilai !");
        }
    }
}
