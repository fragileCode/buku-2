package pengenalanClass;

public class MainApp {

    public static void main(String[] args) {
        Hewan anjing = new Hewan();
        anjing.jenisKelamin = "Jantan";
        anjing.umur = 5;
        anjing.warnaBulu = "Cokelat";

        System.out.println("Anjing " + anjing.jenisKelamin + " berumur "
                + anjing.umur + " tahun berbulu " + anjing.warnaBulu);
        
        //cara memanggi prosedur
        anjing.memakan("Daging");
    }
}
